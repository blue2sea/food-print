// food_print.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols


// Cfood_printApp:
// See food_print.cpp for the implementation of this class
//

class Cfood_printApp : public CWinApp
{
public:
	Cfood_printApp();

// Overrides
	public:
	virtual BOOL InitInstance();

// Implementation

	DECLARE_MESSAGE_MAP()
};

extern Cfood_printApp theApp;