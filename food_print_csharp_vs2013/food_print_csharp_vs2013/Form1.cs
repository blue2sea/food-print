﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace food_print_csharp_vs2013
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            //this.WindowState = FormWindowState.Minimized;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.MinimumSize = new Size(300, 300);
            this.MaximumSize = new Size(300, 300);
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            {
                this.Hide();
                notifyIcon1.Visible = true;
            }
        }

        private void foodCategoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Food_Category_Form form = new Food_Category_Form();
            form.Show();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            About_Form form = new About_Form();
            form.Show();
        }

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Setting_Form form = new Setting_Form();
            form.Show();
        }

        private void logToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.Application.Exit();
        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            {
                this.Show();
                this.WindowState = FormWindowState.Normal;
            }
        }

        private void connect_to_db(object sender, EventArgs e)
        {
            connect_button.Enabled = false;
            String conn_str = "Server=inactive.net;Database=db_golf_2;Username=golf-print;password=GHfads78";
            MySqlConnection conn = new MySqlConnection(conn_str);

            try
            {
                conn.Open();
                MessageBox.Show("Connection Successful!", "Connection Test", MessageBoxButtons.OK);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                MessageBox.Show("Connection Failed!", "Connection Test", MessageBoxButtons.OK);
            }
            connect_button.Enabled = true;
        }

        private void print_food_order(object sender, EventArgs e)
        {

        }
    }
}
