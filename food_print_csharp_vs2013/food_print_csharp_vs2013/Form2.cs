﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace food_print_csharp_vs2013
{
    public partial class Food_Category_Form : Form
    {
        public Food_Category_Form()
        {
            InitializeComponent();
        }

        private void Food_Category_Form_Load(object sender, EventArgs e)
        {
            this.MinimumSize = new Size(500, 500);
            this.MaximumSize = new Size(500, 500);
        }
    }
}
