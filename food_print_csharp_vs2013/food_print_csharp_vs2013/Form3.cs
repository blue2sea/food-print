﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace food_print_csharp_vs2013
{
    public partial class Setting_Form : Form
    {
        public Setting_Form()
        {
            InitializeComponent();
        }

        private void Setting_Form_Load(object sender, EventArgs e)
        {
            this.MinimumSize = new Size(500, 500);
            this.MaximumSize = new Size(500, 500);
        }
    }
}
