﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace food_print_csharp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            this.WindowState = FormWindowState.Minimized;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            {
                Hide();
                notifyIcon1.Visible = true;
            }
        }

        private void notifyicon_dblclick(object sender, MouseEventArgs e)
        {
            Show();
            notifyIcon1.Visible = false;
            WindowState = FormWindowState.Normal;
        }

        private void notifyicon_click(object sender, MouseEventArgs e)
        {
            contextMenuStrip1.Show();
        }

        private void FoodCategory_click(object sender, EventArgs e)
        {
            Food_Category_Form food_cate_form = new Food_Category_Form();
            food_cate_form.Show();
        }

        private void exit(object sender, EventArgs e)
        {
            System.Windows.Forms.Application.Exit();
        }

        private void Settings_click(object sender, EventArgs e)
        {
            Settings_Form setting_form = new Settings_Form();
            setting_form.Show();
        }

    }
}
